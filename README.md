# CI/CD Onboarding workshop

TEST 2! This repository features a first experience & workshop on Continuous Delivery, and will have you performing various activities within a deployment pipeline.
It features a small NodeJS application and corresponding testing scripts to help us do so. 

## Goals

Get familiar with CI/CD concepts by building a pipeline, containing functional testing, non-functionals like performance testing, security validation 
as well as syntactical checks.

If at any time you have questions, feel free to ask in the workshop, contact the workshop host afterwards or raise an issue on this repository. Enjoy!

## Where to begin?

Start out with exercise 1 in the list below. From there, you'll be able to continue to the rest of the exercises. We'd recommend you open a tab for the exercise instructions, and a second tab where you actually make changes to files as necessary. 

## Exercises

1. [Setup GIT](exercises/setup-git-exercise.md)
2. [Setup CI/CD environment](exercises/setup-cicd.md) 
3. [Syntax checking](exercises/syntax-checking.md)
4. [Building and package validation](exercises/code-build.md)
5. [Functional testing](exercises/functional-testing.md)
6. [(Mock) deploy](exercises/mock-deploy.md)
7. [Smoke testing](exercises/smoke-test.md)
8. [User acceptance testing](exercises/user-acceptance-testing.md)
9. [Performance testing](exercises/performance-test.md)

## Editing files

This workshop only requires a browser. When working on files, this can be done using the Gitlab editor.

For the more experienced engineers: you can also clone your personal fork (see [Setup GIT](exercises/setup-git-exercise.md)) to your laptop and work on it with IntelliJ, VSE or similar tools.

Test 
From there, you can use Git to make changes and push them back to Gitlab.

**Note:** ***This workshop was developed for specific purposes; please do not share it publicly.***  

bla
